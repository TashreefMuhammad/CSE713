# CSE 713 || Task 2 Submission

## Link to unlisted youtube video

[https://youtu.be/Rz4Q035tIxI](https://youtu.be/Rz4Q035tIxI)

## Public link to the slide

[Task 2 || Team 04 || Presentation](https://docs.google.com/presentation/d/1Yzn-HNAA5KcuqvseSLjrMzPjV1G0ntE7iUJwMKuHX5s/edit?usp=sharing)

## Paper title

A Survey on Hate Speech Detection using Natural Language Processing

## Link to the paper

[https://aclanthology.org/W17-1101/](https://aclanthology.org/W17-1101/)

## Group Number

04

## Group Members

|ID        | Name                   |
|----------|------------------------|
| 22366003 | Sadia Tasnim           |
| 22366013 | Tashreef Muhammad      |
| 22366032 | Khandaker Rezwan Ahmed |
