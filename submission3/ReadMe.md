# CSE 713 || Task 3 Submission

## Link to unlisted youtube video

[https://youtu.be/RfX6PF2Lq2Y](https://youtu.be/RfX6PF2Lq2Y)

## Two public link to the Concept Map

### PDF

[https://drive.google.com/file/d/1zDWW8YjqPjn0fZyM4ydqzn_xG3Y-dr4A/view?usp=sharing](https://drive.google.com/file/d/1zDWW8YjqPjn0fZyM4ydqzn_xG3Y-dr4A/view?usp=sharing)

### Original File

[https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&layers=1&nav=1&title=Task3ConceptMap#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1MFvB49fqyqQ-oagV2e9e6gcVx3PXH6RD%26export%3Ddownload](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&layers=1&nav=1&title=Task3ConceptMap#Uhttps%3A%2F%2Fdrive.google.com%2Fuc%3Fid%3D1MFvB49fqyqQ-oagV2e9e6gcVx3PXH6RD%26export%3Ddownload)

## Name, Author, Edition, year and publisher of the Book

Pattern Recognition and Machine Learning
Christopher M. Bishop
2006
Springer

## Link to the book

[Pattern Recognition and Machine Learning](https://www.microsoft.com/en-us/research/uploads/prod/2006/01/Bishop-Pattern-Recognition-and-Machine-Learning-2006.pdf)

## Link to the chapter

[Chapter 5: Neural Networks](https://www.microsoft.com/en-us/research/uploads/prod/2006/01/Bishop-Pattern-Recognition-and-Machine-Learning-2006.pdf#page=245)

## Group Number

04

## Group Members

|ID        | Name                   |
|----------|------------------------|
| 22366003 | Sadia Tasnim           |
| 22366013 | Tashreef Muhammad      |
| 22366032 | Khandaker Rezwan Ahmed |
